///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

extern int addCat( char[], enum Gender, enum Breed, bool, float );

