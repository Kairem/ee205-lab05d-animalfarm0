///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "addCats.h"

int addCat( char n[], enum Gender g, enum Breed b, bool fix, float w) {

   bool isUnique = true;
   for(int i = 0; i < numCats; i++) {

      if( strcmp(nameArr[i], n) == 0 ) {
         isUnique = false;
         break;
      }
      else {
         isUnique = true;
      }

   }


   if( strlen(n) < 1 || strlen(n) > 30 || w <= 0 || numCats == MAX_CAT || isUnique == false) {
      exit(EXIT_FAILURE);
   }

   else {
      strcpy(nameArr[numCats], n);
      genderArr[numCats] = g;
      breedArr[numCats] = b;
      isFixedArr[numCats] = fix;   //@todo fix bug with these arrays that is not allowing compile. Possibly with pointers.
      weightArr[numCats] = w;      //flag message: " subscripted value is neither array nor pointer nor vector"

      numCats++; //increment the index of the arrays.
   }

   return numCats;

}

