###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Kai Matsusaka <kairem@hawaii.edu>
### @date 20_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall -Wextra

TARGET = animalFarm0

all: $(TARGET)

addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

catDatabase.o: catDatabase.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c catDatabase.c

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalFarm0: addCats.o reportCats.o updateCats.o deleteCats.o catDatabase.o main.o
	$(CC) $(CFLAGS) -o $(TARGET) addCats.o reportCats.o updateCats.o deleteCats.o catDatabase.o main.o

clean:
	rm -f $(TARGET) *.o

test: animalFarm0
	./animalFarm0
