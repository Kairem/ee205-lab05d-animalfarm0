///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "updateCats.h"

void updateCatName( int index, char newName[] ) {
   
   bool isUnique = true;
   for(int i = 0; i < numCats; i++) {

      if( nameArr[i] == newName ) {
         isUnique = false;
         break;
      }
      else {
         isUnique = true;
      }
   }

   if( strlen(newName) < 0 || strlen(newName) > MAX_CAT_NAME || isUnique == false || index < 0 || index > MAX_CAT - 1 ) {
      exit( EXIT_FAILURE );
   }

   else {
      strcpy(nameArr[index], newName);
   }

}


void fixCat( int index ) {

   if( index < 0 || index > MAX_CAT - 1 ) {
      exit( EXIT_FAILURE );
   }
   
   else {
      isFixedArr[index] = true;
   }

}


void updateCatWeight( int index, float newWeight ) {
   
   if( index < 0 || index > MAX_CAT - 1 || newWeight <= 0 ) {
      exit( EXIT_FAILURE );
   }
   
   else {
      weightArr[index] = newWeight;
   }

}



