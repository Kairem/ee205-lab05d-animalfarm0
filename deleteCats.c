///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"

void deleteAllCats() {
   numCats = 0;        //Set high water mark to 0
}

//add function deleteCat(index) that removes an index from the arrays

