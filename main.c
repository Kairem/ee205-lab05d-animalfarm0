///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

int main() {

addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCat( "Milo", MALE, MANX, true, 7.0 ) ;
addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
printAllCats();

int kali = findCat( "Kali" ) ;

printf("Kali's index: [%d] \n", kali); //@todo convert this to debug statement;

printCat( kali );
printf("\n");
updateCatName( kali, "Capulet" ) ;
updateCatWeight( kali, 9.9 ) ;
fixCat( kali ) ;
printCat( kali );
printf("\n");
printAllCats();
deleteAllCats();
printAllCats();
printAllCats();
}
