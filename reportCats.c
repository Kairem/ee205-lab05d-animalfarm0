///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "reportCats.h"
#include "catDatabase.h"


void printCat(int index) {

   if( index < 0 || index > numCats ) {
      printf( "animalFarm0: Bad Cat [%d]\n", index );
   }

   else {
      printf( "cat index = [%d]  ", index );
      printf( "name = [%s]  ",      nameArr[index] );       //@todo fix bug with these arrays that is not allowing compile. Possibly with pointers.
      printf( "gender = [%d]  ",    genderArr[index] );     //flag message: " subscripted value is neither array nor pointer nor vector"
      printf( "breed = [%d]  ",     breedArr[index] );
      printf( "isFixed = [%d]  ",   isFixedArr[index] );
      printf( "weight = [%f]  ",    weightArr[index] );
   }

}


void printAllCats() {

   for(int i = 0; i < numCats; i++) {
      printf( "cat index = [%d]  ", i );
      printf( "name = [%s]  ",      nameArr[i] );        //Same error here
      printf( "gender = [%d]  ",    genderArr[i] );
      printf( "breed = [%d]  ",     breedArr[i] );
      printf( "isFixed = [%d]  ",   isFixedArr[i] );
      printf( "weight = [%f]  ",    weightArr[i] );
      printf( "\n" );
   }

}


int findCat( char n[] ) {

   for( int i = 0; i < numCats; i++ ) {
      if ( strcmp(nameArr[i], n) == 0 ) {
         return i;
      }
   }

   exit(EXIT_FAILURE);

}


