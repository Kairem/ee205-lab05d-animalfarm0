///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////


extern void printCat( int );

extern void printAllCats();

extern int findCat( char[] );


