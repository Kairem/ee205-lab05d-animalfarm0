///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 20_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#define MAX_CAT 30
#define MAX_CAT_NAME 32

enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

extern char nameArr[MAX_CAT][MAX_CAT_NAME];

extern enum Gender genderArr[MAX_CAT];

extern enum Breed breedArr[MAX_CAT];

extern bool isFixedArr[MAX_CAT];

extern float weightArr[MAX_CAT];

extern int numCats;


//@todo add initialize database funciton that memsets the arrays to zero

